ANSIBLE-PLAYBOOK(1)                                         System administration commands                                         ANSIBLE-PLAYBOOK(1)

NNAAMMEE
       ansible-playbook - Runs Ansible playbooks, executing the defined tasks on the targeted hosts.

SSYYNNOOPPSSIISS
       uussaaggee:: aannssiibbllee--ppllaayybbooookk [[--hh]] [[----vveerrssiioonn]] [[--vv]] [[----pprriivvaattee--kkeeyy PPRRIIVVAATTEE__KKEEYY__FFIILLEE]]
              [-u  REMOTE_USER] [-c CONNECTION] [-T TIMEOUT] [--ssh-common-args SSH_COMMON_ARGS] [--sftp-extra-args SFTP_EXTRA_ARGS] [--scp-extra-args
              SCP_EXTRA_ARGS]  [--ssh-extra-args  SSH_EXTRA_ARGS]  [-k  |  --connection-password-file   CONNECTION_PASSWORD_FILE]   [--force-handlers]
              [--flush-cache]  [-b] [--become-method BECOME_METHOD] [--become-user BECOME_USER] [-K | --become-password-file BECOME_PASSWORD_FILE] [-t
              TAGS] [--skip-tags SKIP_TAGS] [-C]  [--syntax-check]  [-D]  [-i  INVENTORY]  [--list-hosts]  [-l  SUBSET]  [-e  EXTRA_VARS]  [--vault-id
              VAULT_IDS]  [--ask-vault-password | --vault-password-file VAULT_PASSWORD_FILES] [-f FORKS] [-M MODULE_PATH] [--list-tasks] [--list-tags]
              [--step] [--start-at-task START_AT_TASK] playbook [playbook ...]

DDEESSCCRRIIPPTTIIOONN
       the  tool  to  run  _A_n_s_i_b_l_e  _p_l_a_y_b_o_o_k_s,  which  are  a  configuration  and  multinode  deployment  system.   See  the  project  home  page   (‐
       _h_t_t_p_s_:_/_/_d_o_c_s_._a_n_s_i_b_l_e_._c_o_m) for more information.

CCOOMMMMOONN OOPPTTIIOONNSS
          Playbook(s)

       ----aasskk--vvaauulltt--ppaasssswwoorrdd, ----aasskk--vvaauulltt--ppaassss
          ask for vault password

       ----bbeeccoommee--mmeetthhoodd 'BECOME_METHOD'
          privilege escalation method to use (default=sudo), use _a_n_s_i_b_l_e_-_d_o_c _-_t _b_e_c_o_m_e _-_l to list valid choices.

       ----bbeeccoommee--ppaasssswwoorrdd--ffiillee 'BECOME_PASSWORD_FILE', ----bbeeccoommee--ppaassss--ffiillee 'BECOME_PASSWORD_FILE'
          Become password file

       ----bbeeccoommee--uusseerr 'BECOME_USER'
          run operations as this user (default=root)

       ----ccoonnnneeccttiioonn--ppaasssswwoorrdd--ffiillee 'CONNECTION_PASSWORD_FILE', ----ccoonnnn--ppaassss--ffiillee 'CONNECTION_PASSWORD_FILE'
          Connection password file

       ----fflluusshh--ccaacchhee
          clear the fact cache for every host in inventory

       ----ffoorrccee--hhaannddlleerrss
          run handlers even if a task fails

       ----lliisstt--hhoossttss
          outputs a list of matching hosts; does not execute anything else

       ----lliisstt--ttaaggss
          list all available tags

       ----lliisstt--ttaasskkss
          list all tasks that would be executed

       ----pprriivvaattee--kkeeyy 'PRIVATE_KEY_FILE', ----kkeeyy--ffiillee 'PRIVATE_KEY_FILE'
          use this file to authenticate the connection

       ----ssccpp--eexxttrraa--aarrggss 'SCP_EXTRA_ARGS'
          specify extra arguments to pass to scp only (e.g. -l)

       ----ssffttpp--eexxttrraa--aarrggss 'SFTP_EXTRA_ARGS'
          specify extra arguments to pass to sftp only (e.g. -f, -l)

       ----sskkiipp--ttaaggss
          only run plays and tasks whose tags do not match these values

       ----sssshh--ccoommmmoonn--aarrggss 'SSH_COMMON_ARGS'
          specify common arguments to pass to sftp/scp/ssh (e.g. ProxyCommand)

       ----sssshh--eexxttrraa--aarrggss 'SSH_EXTRA_ARGS'
          specify extra arguments to pass to ssh only (e.g. -R)

       ----ssttaarrtt--aatt--ttaasskk 'START_AT_TASK'
          start the playbook at the task matching this name

       ----sstteepp
          one-step-at-a-time: confirm each task before running

       ----ssyynnttaaxx--cchheecckk
          perform a syntax check on the playbook, but do not execute it

       ----vvaauulltt--iidd
          the vault identity to use

       ----vvaauulltt--ppaasssswwoorrdd--ffiillee, ----vvaauulltt--ppaassss--ffiillee
          vault password file

       ----vveerrssiioonn
          show program's version number, config file location, configured module search path, module location, executable location and exit

       --CC, ----cchheecckk
          don't make any changes; instead, try to predict some of the changes that may occur

       --DD, ----ddiiffff
          when changing (small) files and templates, show the differences in those files; works great with --check

       --KK, ----aasskk--bbeeccoommee--ppaassss
          ask for privilege escalation password

       --MM, ----mmoodduullee--ppaatthh
          prepend colon-separated path(s) to module library (default={{ ANSIBLE_HOME ~ "/plugins/modules:/usr/share/ansible/plugins/modules" }})

       --TT 'TIMEOUT', ----ttiimmeeoouutt 'TIMEOUT'
          override the connection timeout in seconds (default=10)

       --bb, ----bbeeccoommee
          run operations with become (does not imply password prompting)

       --cc 'CONNECTION', ----ccoonnnneeccttiioonn 'CONNECTION'
          connection type to use (default=smart)

       --ee, ----eexxttrraa--vvaarrss
          set additional variables as key=value or YAML/JSON, if filename prepend with @

       --ff 'FORKS', ----ffoorrkkss 'FORKS'
          specify number of parallel processes to use (default=5)

       --hh, ----hheellpp
          show this help message and exit

       --ii, ----iinnvveennttoorryy, ----iinnvveennttoorryy--ffiillee
          specify inventory host path or comma separated host list. --inventory-file is deprecated

       --kk, ----aasskk--ppaassss
          ask for connection password

       --ll 'SUBSET', ----lliimmiitt 'SUBSET'
          further limit selected hosts to an additional pattern

       --tt, ----ttaaggss
          only run plays and tasks tagged with these values

       --uu 'REMOTE_USER', ----uusseerr 'REMOTE_USER'
          connect as this user (default=None)

       --vv, ----vveerrbboossee
          Causes  Ansible  to  print more debug messages. Adding multiple -v will increase the verbosity, the builtin plugins currently evaluate up to
          -vvvvvv. A reasonable level to start is -vvv, connection debugging might require -vvvv.

EENNVVIIRROONNMMEENNTT
       The following environment variables may be specified.

       ANSIBLE_CONFIG -- Specify override location for the ansible config file

       Many more are available for most options in ansible.cfg

       For a full list check _h_t_t_p_s_:_/_/_d_o_c_s_._a_n_s_i_b_l_e_._c_o_m_/. or use the _a_n_s_i_b_l_e_-_c_o_n_f_i_g command.

FFIILLEESS
       /etc/ansible/ansible.cfg -- Config file, used if present

       ~/.ansible.cfg -- User config file, overrides the default config if present

       ./ansible.cfg -- Local config file (in current working directory) assumed to be 'project specific' and overrides the rest if present.

       As mentioned above, the ANSIBLE_CONFIG environment variable will override all others.

AAUUTTHHOORR
       Ansible was originally written by Michael DeHaan.

CCOOPPYYRRIIGGHHTT
       Copyright © 2018 Red Hat, Inc | Ansible.  Ansible is released under the terms of the GPLv3 license.

SSEEEE AALLSSOO
       aannssiibbllee (1), aannssiibbllee--ccoonnffiigg (1), aannssiibbllee--ccoonnssoollee (1), aannssiibbllee--ddoocc (1), aannssiibbllee--ggaallaaxxyy  (1),  aannssiibbllee--iinnvveennttoorryy  (1),  aannssiibbllee--ppuullll  (1),  aannssii‐‐
       bbllee--vvaauulltt (1)

       Extensive  documentation  is  available  in the documentation site: <_h_t_t_p_s_:_/_/_d_o_c_s_._a_n_s_i_b_l_e_._c_o_m>.  IRC and mailing list info can be found in file
       CONTRIBUTING.md, available in: <_h_t_t_p_s_:_/_/_g_i_t_h_u_b_._c_o_m_/_a_n_s_i_b_l_e_/_a_n_s_i_b_l_e>

Ansible 2.14.3                                                                                                                     ANSIBLE-PLAYBOOK(1)
